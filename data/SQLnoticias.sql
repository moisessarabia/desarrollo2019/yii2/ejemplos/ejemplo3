﻿CREATE DATABASE noticias;
USE noticias;

CREATE TABLE noticias(
  id int AUTO_INCREMENT NOT NULL,
  titulo varchar(100),
  texto text 
);