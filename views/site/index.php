<?php
use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
 
?>
<div class="site-index">
    <div class="jumbotron fondogris">
        <h1>Ejemplo1 de aplicación</h1>
        <p class="lead">Podemos ver un ejemplo del funcionamiento de este framework</p>
    </div>
    <div class="body-content">
        <div class="container">
            <div class="row">
                <div class="noticias">
                    <?= GridView::widget([
    'dataProvider' => $dataProvider,
]);
?>
                </div>
            </div>
        </div>
    </div>
</div>
